module com.example.softwarearchitektur {
    requires javafx.controls;
    requires javafx.fxml;
    requires io.reactivex.rxjava3;
	requires jdk.jdeps;


    opens com.example.softwarearchitektur to javafx.fxml;
    exports com.example.softwarearchitektur;
    exports com.example.softwarearchitektur.model;
    opens com.example.softwarearchitektur.model to javafx.fxml;
    exports com.example.softwarearchitektur.ui.fx;
    opens com.example.softwarearchitektur.ui.fx to javafx.fxml;
    exports com.example.softwarearchitektur.ui;
    opens com.example.softwarearchitektur.ui to javafx.fxml;
    exports com.example.softwarearchitektur.repo;
    opens com.example.softwarearchitektur.repo to javafx.fxml;

}