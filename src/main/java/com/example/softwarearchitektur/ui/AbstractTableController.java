package com.example.softwarearchitektur.ui;

import com.example.softwarearchitektur.repo.EntryRepo;
import com.example.softwarearchitektur.repo.StationRepo;
import com.example.softwarearchitektur.model.Entry;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * The {@link AbstractTableController} implements {@link Observer} and should be registered in the {@link EntryRepo}.
 */
public abstract class AbstractTableController implements Observer<Entry> {
    private final EntryRepo entryRepo;

    public AbstractTableController() {
        this.entryRepo = EntryRepo.getInstance();
        this.entryRepo.subscribe(this);
    }

    protected abstract void addRow(Entry entry);

    /**
     * Initializes the table by adding all entries in {@link EntryRepo} to the table.
     */
    public void setup() {
        for (Entry e : entryRepo.getEntries()) {
            addRow(e);
        }
    }

    @Override
    public void onSubscribe(@NonNull Disposable d) {}

    /**
     * Whenever the observable has new entry, it is added to the table
     * @see Observer
     * @param entry The entry to be added to the table
     */
    @Override
    public void onNext(@NonNull Entry entry) {
        addRow(entry);
    }

    /**
     * Errors in the observable pattern are printed to the console as they should never occur.
     * @param e The error object
     */
    @Override
    public void onError(@NonNull Throwable e) {
        System.err.println(e.getMessage());
    }
    /**
     * Notifies the Observer that the Observable has finished sending push-based notifications. 
     */
    @Override
    public void onComplete() {}
}
