package com.example.softwarearchitektur.ui;

import com.example.softwarearchitektur.repo.EntryRepo;
import com.example.softwarearchitektur.repo.StationRepo;
import com.example.softwarearchitektur.model.Entry;
import com.example.softwarearchitektur.model.Station;
import io.reactivex.rxjava3.core.Observer;

import java.time.LocalDate;

public abstract class AbstractMainController {
	
	private final EntryRepo entryRepo;
	private final StationRepo stationRepo;
	
	public AbstractMainController() {
		this.stationRepo = StationRepo.getInstance();
		this.entryRepo = EntryRepo.getInstance();
	}

	abstract protected void addStationToList(String stationId);

	abstract protected String getSelectedStationId();

	abstract protected LocalDate getDate();

	abstract protected int getActual();

	abstract protected void setStationId(String stationId);

	abstract protected void setTarget(String target);

	abstract protected void setVariance(String variance);

	abstract protected void setVarianceColor(String color);

    /**
     * Generates a station and adds it to the {@link StationRepo}.
     */
    protected void generateStation() {
        Station station = stationRepo.addStation();
        addStationToList(station.getId());
    }

    /**
     * Retrieves the selected station from {@link this#getSelectedStationId()} and
     * sets its properties.
     */
    protected void loadSelectedStation() {
    	try {
    		String stationId = getSelectedStationId();
    		Station station = stationRepo.findStation(stationId).orElseThrow();
    		setStationId(stationId);    		
    		setTarget(String.valueOf(station.getTarget()));
    	} catch (java.lang.IndexOutOfBoundsException ex) {
    		//System.out.println(ex.getMessage());
    	}
    }

    /**
     * Adds a new entry to the {@link EntryRepo} and
     * updates the variance.
     */
    protected void addEntry() {
        Entry entry = entryRepo.addEntry(
                getSelectedStationId(),
                getDate(),
                getActual());
        setVariance(String.valueOf(entry.getVariance()));
        switch (entry.getColor()) {
            case RED:
                setVarianceColor("red");
                break;
            case GREEN:
                setVarianceColor("green");
                break;
            case STANDARD:
                setVarianceColor("black");
                break;
        }
    }
}
