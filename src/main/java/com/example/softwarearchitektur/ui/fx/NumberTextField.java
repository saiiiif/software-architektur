package com.example.softwarearchitektur.ui.fx;

import javafx.scene.control.TextField;

/**
 * This class implements the number check for the text fields
 */
public class NumberTextField extends TextField {
    public NumberTextField() {
        this.setPromptText("Enter Only Number");

    }

    @Override
    public void replaceText(int i, int il, String string) {
        if (string.matches("[0-9]") || string.isEmpty()) {
            super.replaceText(i, il, string);
        }
    }

    @Override
    public void replaceSelection(String string) {
        super.replaceSelection(string);
    }
}
