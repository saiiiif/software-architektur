package com.example.softwarearchitektur.ui.fx;

import com.example.softwarearchitektur.IceStationApplication;
import com.example.softwarearchitektur.ui.AbstractMainController;
import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.time.LocalDate;

public class MainControllerFxImpl extends AbstractMainController {
    @FXML
    public Button addStationBtn, saveBtn, clearBtn;
    // label for the hidden text
    @FXML
    private Label label;

    //inputs
    @FXML
    private TextField stationId, date, actual, variance, target;

    //Station list view
    @FXML
    private ListView<String> stationList;

    @Override
    protected void addStationToList(String stationId) {
        stationList.getItems().add(stationId);
    }

    @Override
    protected String getSelectedStationId() {
        return stationList.getSelectionModel().getSelectedItems().get(0);
    }

    @Override
    protected LocalDate getDate() {
        return LocalDate.parse(date.getText());
    }

    @Override
    protected int getActual() {
        return Integer.parseInt(actual.getText());
    }

    @Override
    protected void setStationId(String stationId) {
        this.stationId.setText(stationId);
    }

    @Override
    protected void setTarget(String target) {
        this.target.setText(target);
    }

    @Override
    protected void setVariance(String variance) {
        this.variance.setText(variance);
    }

    @Override
    protected void setVarianceColor(String color) {
        this.variance.setStyle("-fx-text-inner-color: " + color + ";");
    }

    /**
     * Generates a new station and adds it the view
     * @param event unused
     */
    @FXML
    public void addStation(MouseEvent event) {
        label.setText("");
        generateStation();
        stationId.clear();
    }

    /**
     * Handler for mouse click on station
     * @param event unused
     */
    @FXML
    public void selectStation(MouseEvent event) {
        loadSelectedStation();
    }

    /**
     * Checks that the inputs aren't null or empty
     * @return True if all checks were successful
     */
    private boolean checkInputs() {
        return (stationId.getText() != null
                && !stationId.getText().isEmpty())
                && (date.getText() != null
                && !date.getText().isEmpty())
                && (actual.getText() != null && !actual.getText().isEmpty());
    }

    /**
     * Clears the text fields
     * @param event unused
     */
    @FXML
    public void clearInputs(MouseEvent event) {
        stationId.clear();
        date.clear();
        target.clear();
        actual.clear();
        variance.clear();
    }

    /**
     * Saves a new entry if {@link this#checkInputs()} returns true
     * @param event unused
     */
    @FXML
    public void saveEntry(MouseEvent event) {
        label.setText("");
        try {
            if (checkInputs()) {
                addEntry();
            } else {
                label.setText("Enter Date and Actual value");
            }
        } catch (Exception e) {
            label.setText("Please Enter Correct Date");
        }

    }

    /**
     * Opens a new window with the table view in it.
     * @param mouseEvent unused
     */
    @FXML
    public void showTableView(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(IceStationApplication.class.getResource("table.fxml"));
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
