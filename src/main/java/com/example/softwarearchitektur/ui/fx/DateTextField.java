package com.example.softwarearchitektur.ui.fx;

import javafx.scene.control.TextField;

/**
 * This class implements the date check for the text fields.
 *
 * The date must comply to the pattern YYYY-MM-dd.
 */
public class DateTextField extends TextField {
    public DateTextField() {
        this.setPromptText("Year-Month-Day");

    }

    @Override
    public void replaceText(int i, int il, String string) {
        if (string.matches("[/^[\\d]{4}-[\\d]{2}-[\\d]{2}$/]") || string.isEmpty()) {
            super.replaceText(i, il, string);
        }
    }

    @Override
    public void replaceSelection(String string) {
        super.replaceSelection(string);
    }
}

