package com.example.softwarearchitektur.ui.fx;

import com.example.softwarearchitektur.model.Entry;
import com.example.softwarearchitektur.ui.AbstractTableController;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

public class TableControllerFxImpl extends AbstractTableController {
    @FXML
    public TableView<Entry> table;

    @FXML
    public TableColumn<Entry, String> stationIdColumn;

    @FXML
    public TableColumn<Entry, String> dateColumn;

    @FXML
    public TableColumn<Entry, String> targetColumn;

    @FXML
    public TableColumn<Entry, String> actualColumn;

    @FXML
    public TableColumn<Entry, String> varianceColumn;

    /**
     * Called by JavaFX after the annotated properties are injected and
     * initializes the columns with their corresponding {@link PropertyValueFactory}.
     */
    @FXML
    void initialize() {
        setup();

        stationIdColumn.setCellValueFactory(new PropertyValueFactory<>("stationId"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        targetColumn.setCellValueFactory(new PropertyValueFactory<>("target"));
        actualColumn.setCellValueFactory(new PropertyValueFactory<>("actual"));
        varianceColumn.setCellValueFactory(new PropertyValueFactory<>("variance"));
    }

    /**
     * Adds a new row to the table
     * @param entry The new entry to be added
     */
    @Override
    protected void addRow(Entry entry) {
        table.getItems().add(entry);
    }
}
