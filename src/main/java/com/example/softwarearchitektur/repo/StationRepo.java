package com.example.softwarearchitektur.repo;

import com.example.softwarearchitektur.model.Station;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;

import java.util.*;

/**
 * The {@link StationRepo} generates new stations and stores them in a {@link List}.
 *
 * It is {@link Observable}.
 */
public class StationRepo extends Observable<Station> {
	private static StationRepo instance;
	private final List<Station> stations;
	private final List<Observer<? super Station>> observers;

	private StationRepo() {
		this.observers = new ArrayList<>();
		this.stations = new ArrayList<>();
	}

	public static StationRepo getInstance() {
		if (instance == null) {
			instance = new StationRepo();
		}
		return instance;
	}

	public Station addStation() {
		Station station = new Station();
		stations.add(station);
		for (Observer<? super Station> observer : observers) {
			observer.onNext(station);
		}
		return station;
	}

	public Optional<Station> findStation(String id) {
		return stations.stream().filter(it -> it.getId().equals(id)).findFirst();
	}

	@Override
	protected void subscribeActual(@NonNull Observer<? super Station> observer) {
		observers.add(observer);
	}
}
