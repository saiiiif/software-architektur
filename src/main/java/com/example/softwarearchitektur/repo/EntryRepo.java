package com.example.softwarearchitektur.repo;

import com.example.softwarearchitektur.model.Entry;
import com.example.softwarearchitektur.model.Station;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The {@link EntryRepo} stores entries in a {@link List}.
 *
 * It is {@link Observable}.
 */
public class EntryRepo extends Observable<Entry> {
	private static EntryRepo instance;
	private final StationRepo stationRepo;
	private final List<Entry> entries;
	private final List<Observer<? super Entry>> observers;

	private EntryRepo() {
		this.entries = new ArrayList<>();
		this.observers = new ArrayList<>();
		this.stationRepo = StationRepo.getInstance();
	}

	public static EntryRepo getInstance() {
		if (instance == null) {
			instance = new EntryRepo();
		}
		return instance;
	}

	public Entry addEntry(String stationId, LocalDate date, int actual) {
		Station station = stationRepo.findStation(stationId).orElseThrow();
		Entry entry = new Entry(station, date, actual);
		entries.add(entry);
		for (Observer<? super Entry> observer : observers) {
			observer.onNext(entry);
		}
		return entry;
	}

	public List<Entry> getEntries() {
		return Collections.unmodifiableList(this.entries);
	}

	@Override
	protected void subscribeActual(@NonNull Observer<? super Entry> observer) {
		observers.add(observer);
	}
}
