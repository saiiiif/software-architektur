package com.example.softwarearchitektur.model;

import java.time.LocalDate;

public class Entry {
    private final Station station;
    private final LocalDate date;
    private final int actual;
    private final int variance;
    private final EntryVarianceColor color;

    public Entry(Station station, LocalDate date, int actual) {
        this.station = station;
        this.date = date;
        this.actual = actual;
        this.variance = actual - station.getTarget();

        int target = station.getTarget();
        if (actual < target && (Math.abs(variance) * 100 / target) > 10) {
            color = EntryVarianceColor.RED;
        } else if (actual > target && (Math.abs(variance) * 100 / target) > 5) {
            color = EntryVarianceColor.GREEN;
        } else {
            color = EntryVarianceColor.STANDARD;
        }
    }

    /**
     * Delegate to {@link Station#getId()}.
     * @return Same as {@link Station#getId()}
     */
    public String getStationId() { return station.getId(); }

    /**
     * Delegate to {@link Station#getTarget()} ()}.
     * @return Same as {@link Station#getTarget()}
     */
    public int getTarget() { return station.getTarget(); }

    public Station getStation() {
        return station;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getActual() {
        return actual;
    }

    public int getVariance() {
        return variance;
    }

    public EntryVarianceColor getColor() {
        return color;
    }
}
