package com.example.softwarearchitektur.model;

import java.util.Random;

public class Station {
    private final String id;
    private final int target;
    private final int LEN = 5;
    private final int TARGET_BOUND = 100;

    public Station() {
        this.id = generateRandomId(LEN);
        this.target = generateRandomTarget(TARGET_BOUND);
    }

    public int getTarget() {
        return target;
    }

    public String getId() {
        return id;
    }

	/**
	 * Generates a random ID with a length of {@param len}.
	 * @param len The length of the random ID
	 * @return A String containing the generated ID
	 */
	private static String generateRandomId(int len) {
        String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
                + "lmnopqrstuvwxyz";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString().toUpperCase();
    }

	/**
	 * Generates a random target value for the ice-cream particle concentration.
	 * @param bound The upper bound
	 * @return A random integer in [1, bound)
	 */
	private static int generateRandomTarget(int bound) {
        Random rnd = new Random();
        return rnd.nextInt(bound - 1) + 1;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + LEN;
		result = prime * result + TARGET_BOUND;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + target;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Station other = (Station) obj;
		if (LEN != other.LEN)
			return false;
		if (TARGET_BOUND != other.TARGET_BOUND)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (target != other.target)
			return false;
		return true;
	}
    
    
}
