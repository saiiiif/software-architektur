package com.example.softwarearchitektur.model;

public enum EntryVarianceColor {
    STANDARD, RED, GREEN
}
