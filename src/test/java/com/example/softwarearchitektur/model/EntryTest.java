
package com.example.softwarearchitektur.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.mockito.junit.jupiter.*;

import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EntryTest {

	/**
	 * Werte die beim Testen als Parameter verwendet werden.
	 */
	final String ID = "HOLA";
	final int TARGET = 23;
	final String DATE = "2019-03-29";
	final int ACTUAL = 34;
	
	/**
	 * Ein Mockobject f�r die Stationklasse
	 */
	@Mock
	Station stationMock;

	/**
	 * Testet die Initialisierung von EntryRepo
	 * Pr�ft auch ob alle Felder der EntryRepoklasse richtig initialisiert wurden
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test
	public void entryConstructor() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		when(stationMock.getTarget()).thenReturn(TARGET);
		lenient().when(stationMock.getId()).thenReturn(ID);
		
		LocalDate ld = LocalDate.parse(DATE);
		Entry entry = new Entry(stationMock, ld, ACTUAL);
		
		Field actualF = Entry.class.getDeclaredField("actual");
		Field colorF = Entry.class.getDeclaredField("color");
		Field varianceF = Entry.class.getDeclaredField("variance");
		Field stationF = Entry.class.getDeclaredField("station");
		Field dateF = Entry.class.getDeclaredField("date");
		
		actualF.setAccessible(true);
		colorF.setAccessible(true);
		varianceF.setAccessible(true);
		stationF.setAccessible(true);
		dateF.setAccessible(true);
		
		assertThat(actualF.get(entry)).as("'actual' wurde nicht initialiseirt.").isEqualTo(ACTUAL);
		assertThat(colorF.get(entry)).as("'color' soll initialisert werden.").isNotNull();
		assertThat(varianceF.get(entry)).as("'variance' hat nicht den Richtigen wert.").isEqualTo(ACTUAL- TARGET);
		assertThat(stationF.get(entry)).as("'station' darf nicht null sein.").isNotNull();
		assertThat(dateF.get(entry)).as("'date' wurde nicht korrekt initialisiert.").isEqualTo(ld);
		
	}
	
	/**
	 * Eine Hilfmethode zum Erstellen von Entryobjekten
	 * @param target
	 * @param id
	 * @param ld
	 * @param actual
	 * @return das erstellte Entry-Objekt
	 */
	private Entry createEntry(int target, String id, LocalDate ld, int actual) {
		when(stationMock.getTarget()).thenReturn(target);
		lenient().when(stationMock.getId()).thenReturn(id);
    	return new Entry(stationMock, ld, actual);
	}
	/**
	 * Teste die getStationMethode
	 */
    @Test
    public void getStation(){
		Station station = new Station();
    	Entry entry = new Entry(station, LocalDate.parse(DATE), 23);
    	assertThat(entry.getStation()).isNotNull().isEqualTo(station);
	}

    @Test
    public void getDate() {
    	LocalDate ld = LocalDate.parse(DATE);
    	Entry entry = createEntry(TARGET,ID,ld,ACTUAL);
    	assertThat(ld).isEqualTo(entry.getDate());
    }
    

    @Test
    public void getActual() {
    	LocalDate ld = LocalDate.parse(DATE);
    	Entry entry = createEntry(TARGET,ID,ld,ACTUAL);
    	assertThat(entry.getActual()).isEqualTo(ACTUAL);
    }


    @Test
    public void getVariance() throws Exception {
    	LocalDate ld = LocalDate.parse(DATE);
    	Entry entry = createEntry(TARGET, ID, ld, ACTUAL);
    	assertThat(entry.getVariance()).isEqualTo(ACTUAL- TARGET);
    }

    @Test
    public void getColor() {  	 
    	LocalDate ld = LocalDate.parse(DATE);
    	Entry entry = createEntry(TARGET, ID, ld, ACTUAL);
    	assertThat(entry.getColor()).isNotNull();
    }
    

    
    
    
}