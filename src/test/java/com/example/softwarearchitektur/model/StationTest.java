package com.example.softwarearchitektur.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import org.junit.jupiter.api.Test;

public class StationTest {
	
	/***
	 * Teste ob die Stationklasse Problemlos initialisiert wird und ob die enthaltene Komponente initialisiert werden.
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test
	public void stationInit() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Station station = new Station();

		final Field targetField = Station.class.getDeclaredField("target");
        targetField.setAccessible(true);
        
        final Field idField = Station.class.getDeclaredField("id");
        idField.setAccessible(true);
              
		assertThat((Integer)targetField.get(station)).as("The Target is negative").isGreaterThanOrEqualTo(0);
		assertThat((String)idField.get(station)).as("The ID is Null or Empty").isNotNull().isNotEmpty();
	}
	
	/**
	 * Test f�r die Methode getTarget()
	 */
    @Test
    public void getTarget() {
    	Station station = new Station();
    	assertThat(station.getTarget()).isGreaterThan(0);
    }
    /**
     * Test f�r die Methode getId();
     */
    @Test
    public void getId() {
    	Station station = new Station();
    	assertThat(station.getId()).isNotNull().isNotEmpty();
    }
}