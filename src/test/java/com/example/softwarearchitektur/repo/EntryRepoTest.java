package com.example.softwarearchitektur.repo;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.softwarearchitektur.model.Entry;
import com.example.softwarearchitektur.model.Station;

@ExtendWith(MockitoExtension.class)
public class EntryRepoTest {

	/**
	 * Neue Initialisierung der EntryRepo und die StationRepo
	 * Diese Methode wird vor jedem Test aufgerufen, um sicher zu stellen, dass die Teste keinen Einfluss auf einander haben.
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@BeforeEach
	public void reset()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field entryInstanceField = EntryRepo.class.getDeclaredField("instance");
		entryInstanceField.setAccessible(true);
		entryInstanceField.set(EntryRepo.getInstance(), null);
		Field stationInstanceField = StationRepo.class.getDeclaredField("instance");
		stationInstanceField.setAccessible(true);
		stationInstanceField.set(StationRepo.getInstance(), null);
	}

	/**
	 * Teste die Singleton-design-Pattern
	 * Es wird gepr�ft, ob die Pattern richtig implementiert wurde. (Es darf nur ein Instance von der Klasse EntryRepo geben)
	 */
	@Test
	public void singletonPattern() {
		EntryRepo ep1 = EntryRepo.getInstance();
		EntryRepo ep2 = EntryRepo.getInstance();

		assertThat(ep1).isSameAs(ep2);
	}

	/**
	 * pr�fe ob alle Felder in der Klasse EntryRepo initialisiert werden
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test
	public void fieldsInitialisation()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		EntryRepo sr = EntryRepo.getInstance();

		Field entriesField = EntryRepo.class.getDeclaredField("entries");
		Field observersField = EntryRepo.class.getDeclaredField("observers");
		Field stationRepo = EntryRepo.class.getDeclaredField("stationRepo");

		entriesField.setAccessible(true);
		observersField.setAccessible(true);
		stationRepo.setAccessible(true);

		assertThat(entriesField.get(sr)).isNotNull().asList().isEmpty();
		assertThat(observersField.get(sr)).isNotNull().asList().isEmpty();
		assertThat(stationRepo.get(sr)).isNotNull();

	}

	/**
	 * Es wird getestet, ob ein Entry Problemlos hinzugef�gt werden kann.
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void addEntry()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		LocalDate ld = LocalDate.parse("2021-01-01");
		int actual = 24;

		StationRepo sp = StationRepo.getInstance();
		sp.addStation();
		Field stations = StationRepo.class.getDeclaredField("stations");
		stations.setAccessible(true);

		List<Station> stationslist = (List<Station>) stations.get(sp);
		Station st = stationslist.get(0);

		EntryRepo ep = EntryRepo.getInstance();
		ep.addEntry(st.getId(), ld, actual);

		Entry e = ep.getEntries().get(0);

		assertThat(e.getDate()).isSameAs(ld);
		assertThat(e.getColor()).isNotNull();
		assertThat(e.getActual()).isEqualTo(actual);
		assertThat(e.getStation()).isEqualTo(st);

	}

	/**
	 * Die Methode getEntries wird getestet.
	 * Es werden 100 Entries hinzugef�gt. Danach wird getestet, ob die Entries da sind.
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test
	public void getEntries()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		LocalDate ld = LocalDate.parse("2021-01-01");

		StationRepo sp = StationRepo.getInstance();
		for (int i = 0; i < 100; i++)
			sp.addStation();

		Field stations = StationRepo.class.getDeclaredField("stations");
		stations.setAccessible(true);

		List<Station> stationslist = (List<Station>) stations.get(sp);

		EntryRepo ep = EntryRepo.getInstance();
		for (Station st : stationslist) {
			ep.addEntry(st.getId(), ld, stationslist.indexOf(st));
		}

		assertThat(ep.getEntries()).isNotEmpty().doesNotContainNull().hasSize(100);

	}

}
