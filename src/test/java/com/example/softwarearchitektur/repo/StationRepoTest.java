package com.example.softwarearchitektur.repo;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.softwarearchitektur.model.Station;

@ExtendWith(MockitoExtension.class)
public class StationRepoTest {

	/**
	 * Neue Initialisierung der Klasse StationRepo
	 * Diese Methode wird vor jeder Test aufgeruft, um sicherzustellen das die Testf�lle keinen Einfluss auf einander haben.
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@BeforeEach
	public void reset()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field instanceField = StationRepo.class.getDeclaredField("instance");
		instanceField.setAccessible(true);
		instanceField.set(StationRepo.getInstance(), null);
	}

	/**
	 * Es wird getestet, ob die StationRepoklasse korrekt initialisiert wird.
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test
	public void listsInitialisation()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		StationRepo sr = StationRepo.getInstance();

		Field stationsField = StationRepo.class.getDeclaredField("stations");
		Field observersField = StationRepo.class.getDeclaredField("observers");

		stationsField.setAccessible(true);
		observersField.setAccessible(true);

		assertThat(stationsField.get(sr)).isNotNull().asList().isEmpty();
		assertThat(observersField.get(sr)).isNotNull().asList().isEmpty();
	}
/**
 * Hier wird die Implementierung der SingletonPattern gepr�ft. (Es darf nur ein Objekt der Klasse initialisiert werden)
 */
	@Test
	public void singletonPattern() {
		StationRepo sp1 = StationRepo.getInstance();
		StationRepo sp2 = StationRepo.getInstance();

		assertThat(sp1).isSameAs(sp2);
	}

	/**
	 * Hier wird eine Station hinzugef�gt und wird getestet ob diese Station anhand der FindStationMethode gefunden werden kann.
	 * 
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void findStation()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		Station st = new Station();
		StationRepo sp = StationRepo.getInstance();

		Field stationsField = StationRepo.class.getDeclaredField("stations");
		stationsField.setAccessible(true);
		((List<Station>) stationsField.get(sp)).add(st);

		assertThat(sp.findStation(st.getId()).isPresent()).isTrue();
		assertThat(sp.findStation(st.getId()).get()).isEqualTo(st);

	}
/**
 * Hier wird die Methode addStation getestet
 * @throws NoSuchFieldException
 * @throws SecurityException
 * @throws IllegalArgumentException
 * @throws IllegalAccessException
 */
	@SuppressWarnings("unchecked")
	@Test
	public void addingStations()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		StationRepo sp = StationRepo.getInstance();
		Field stationsField = StationRepo.class.getDeclaredField("stations");
		stationsField.setAccessible(true);
		for (int i = 0; i < 10; i++) {
			sp.addStation();
		}

		List<Station> stations = (List<Station>) stationsField.get(sp);
		assertThat(stations).isNotNull().isNotEmpty().asList().hasSize(10);

	}

}
